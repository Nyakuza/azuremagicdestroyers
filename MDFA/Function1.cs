using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using MDFA.Characters;
using System.Collections.Generic;
using MDFA.Characters.Melee;
using MDFA.Characters.Arcane;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Net.Http;

namespace MDFA
{

    public static class Function1
    {

        private static readonly HttpClient client = new HttpClient();

        [FunctionName("MDMain")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {

            try
            {
            
                log.LogInformation("C# HTTP trigger function processed a request.");

                //string name = req.Query["name"];

                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                dynamic data = JsonConvert.DeserializeObject(requestBody);
                ///name = name ?? data?.name;

                Console.WriteLine("MDFA SUCCESS; requestBody: " + requestBody);
                log.LogTrace("MDFA SUCCESS; requestBody: " + requestBody);

                // logger reference
                // log.LogInformation("_TEST_TEST_TEST_TEST_TEST_TEST_TEST");
                // log.LogTrace("Here is a verbose log message");
                // log.LogWarning("Here is a warning log message");
                // log.LogError("Here is an error log message");
                // log.LogCritical("This is a critical log message => {message}", "We have a big problem");

                //System.Text.Json.Nodes.JsonObject stuff = (System.Text.Json.Nodes.JsonObject)JsonConvert.DeserializeObject(requestBody);
                //dynamic stuff = JsonConvert.DeserializeObject(requestBody);

                // deserialize json into wrapper objects
                List<Wrapper> charListWrapper = new List<Wrapper>();
                charListWrapper = JsonConvert.DeserializeObject<List<Wrapper>>(requestBody, new Newtonsoft.Json.JsonSerializerSettings
                {
                    TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Auto,
                    NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
               });
                //charListDirect = JsonConvert.DeserializeObject<List<CharacterBase>>(requestBody, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
 
                // unwrap the input object list and sort the characters into two teams
                Tuple<List<CharacterBase>, List<CharacterBase>> teams = GameService.FillTeams(charListWrapper);
                List<CharacterBase> team_1 = teams.Item1;
                List<CharacterBase> team_2 = teams.Item2;

                // run the game simulation
                string outcome = GameService.Play(team_1, team_2);  

                // form json string for the DB record function
                var values = new Dictionary<string, string>
                    {
                        { "RESULT", outcome },
                        { "TEAMS", requestBody }
                    };

                // send game details to DB record function
                var json = JsonConvert.SerializeObject(values, Formatting.Indented);
                var stringContent = new StringContent(json);
                var response = await client.PostAsync("https://nestedfa.azurewebsites.net/api/HttpTrigger1?code=wPY5S5TLAtP8aH3eeubcdO1yqs0O02A2Pa2nIsOXEEnXPAm64MMvpA==", stringContent);
                var responseString = await response.Content.ReadAsStringAsync();
            
                // return to Logic App
                return new OkObjectResult("MDFA SUCCESS: " + outcome + " TEAMS: " + requestBody);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
                Tools.ColorfulWriteLine(ex.ToString(), ConsoleColor.Red);
                return new ObjectResult(ex.ToString());
            }

            // sample output was: MDFA SUCCESS: [{"Name":"Jack","Level":3,"Class":"Mage"},{"Name":"Jack","Level":3,"Class":"Mage"},{"Name":"Jack","Level":3,"Class":"Mage"},{"Name":"Jack","Level":3,"Class":"Mage"},{"Name":"Jack","Level":3,"Class":"Mage"},{"Name":"Jack","Level":3,"Class":"Mage"}]
        
        }
    }
}
