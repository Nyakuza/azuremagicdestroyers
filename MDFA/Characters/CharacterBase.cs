
using Equipment;
using Newtonsoft.Json;
using System;

namespace MDFA.Characters
{
    
    

    public abstract class CharacterBase
    {


        public static int counter = 0;
        private readonly int id;
        [JsonProperty(PropertyName = "JSONHealthPoints")]
        private int healthPoints;
        [JsonProperty(PropertyName = "JSONLevel")]
        private int level;
        private bool isAlive;
        private int score;
        [JsonProperty(PropertyName = "JSONName")]
        private String name = "Default";
        private CharType chartype = 0;
        [JsonProperty(PropertyName = "JSONC_Class")]
        private String c_class = "Warrior";
        private Armor armor;
        private Weapon weapon;
        public int Id { get { return id; } }

         public Weapon Weapon
        {
            get
            {
                return this.weapon;
            }

            set
            {
                this.weapon = value;
            }
        }

        public Armor Armor
        {
            get
            {
                return this.armor;
            }

            set
            {
                this.armor = value;
            }
        }

        public CharType CharType
        {
            get
            {
                return this.chartype;
            }

            set
            {
                this.chartype = value;
            }
        }

        public String C_class
        {
            get
            {
                return this.c_class;
            }

            set
            {
                this.c_class = value;
            }
        }

        public bool IsAlive
        {
            get
            {
                return this.isAlive;
            }

            protected set
            {
                this.isAlive = value;
            }
        }

        public int HealthPoints
        {
            get
            {
                return this.healthPoints;
            }

            set
            {
                if (value >= 0 && value <= 120)
                {
                    this.healthPoints = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(string.Empty, "Inappropriate value, the value should be >= 0 and <= 100.");
                }
            }
        }

        public int Level
        {
            get
            {
                return this.level;
            }

            set
            {
                if (value >= 0)
                {
                    this.level = value;
                }
                else
                {
                    throw new ArgumentOutOfRangeException(string.Empty, "Inappropriate value, level should always be positive.");
                }
            }
        }

        public int Score
        {
            get
            {
                return this.score;
            }

            set
            {
                this.score = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }

            set
            {
                if (value.Length >= 3 && value.Length <= 12)
                {
                    this.name = value;
                }
                else
                {
                    throw new ArgumentException(string.Empty, "Inappropriate length of name, name should be between 3 and 12 characters.");
                }
            }
        }

        protected void CreationCofirmation() {

            Tools.TypeSpecificColorfulCW("Character " + this.Name + " the " + this.GetType().Name.ToString() + " Created!", this.GetType().ToString());
            System.Console.WriteLine(this.GetType().ToString());
        }

        public virtual void Move()
        {
            Console.WriteLine("Moving");

        }

        public abstract int Attack();

        public abstract int SpecialAttack();

        public abstract int Defend();

        public void TakeDamage(int damage, string attackerName, string type)
        {
            if (this.Defend() < damage)
            {
                this.healthPoints = this.healthPoints - damage + this.Defend();

                if (this.healthPoints <= 0)
                {
                    this.isAlive = false;
                }
            }
            else
            {
                Console.WriteLine("Haha! Your damage was not enough to harm me!");
            }

            if (!this.isAlive)
            {
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
                Console.WriteLine($"{this.name} received {damage} damage from {attackerName}, and is now dead!");
                Console.ResetColor();
            }
            else
            {
                Tools.TypeSpecificColorfulCW($"{this.name} received {damage} damage from {attackerName}, and now has {this.healthPoints} healthpoints!", type);
            }
        }

        public void WonBattle()
        {
            this.score++;

            if (this.score % 10 == 0)
            {
                this.level++;
            }
        }

    }
}