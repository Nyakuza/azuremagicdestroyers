using Equipment;
using System;

namespace MDFA.Characters.Arcane
{
    public class Necromancer : Arcane
    {
        
        private readonly Armor DEFAULT_BODY_ARMOR = new Robe();
        private readonly Staff DEFAULT_WEAPON = new Staff();
        
        public Necromancer()
            : this(Consts.Necromancer.NAME, 1)
        {
        }

        public Necromancer(string name, int level)
            : this(name, level, Consts.Necromancer.HEALTH_POINTS)
        {
        }

        public Necromancer(string name, int level, int healthPoints)
        {
            base.Name = name;
            base.Level = level;
            base.HealthPoints = healthPoints;
            base.ManaPoints = Consts.Necromancer.MANA_POINTS;
            base.Armor = DEFAULT_BODY_ARMOR;
            base.Weapon = DEFAULT_WEAPON;
            base.CharType = CharType.Arcane;
            base.IsAlive = true;
            base.Score = 0;

            counter++;
            CreationCofirmation();
        }


        // abstract method implementations
        public override int Attack()
        {
            return this.ShadowRaze();
        }

        public override int SpecialAttack()
        {
            return this.VampireTouch();
        }

        public override int Defend()
        {
            return this.BoneShield();
        }


        // class abilities
        public int ShadowRaze()
        {
            return base.Weapon.Damage + 10;
        }

        public int VampireTouch()
        {
            throw new NotImplementedException();
        }

        public int BoneShield()
        {
            return base.Armor.ArmorPoints + 5;
        }

    }
}