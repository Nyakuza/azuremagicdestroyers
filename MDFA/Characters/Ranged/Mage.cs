using Equipment;
using System;

namespace MDFA.Characters.Arcane
{
    public class Mage : Arcane
    {
        
        private readonly Armor DEFAULT_BODY_ARMOR = new Robe();
        private readonly Staff DEFAULT_WEAPON = new Staff();
        
        public Mage()
            : this(Consts.Mage.NAME, 1)
        {
        }

        public Mage(string name, int level)
            : this(name, level, Consts.Mage.HEALTH_POINTS)
        {
        }

        public Mage(string name, int level, int healthPoints)
        {
            base.Name = name;
            base.Level = level;
            base.HealthPoints = healthPoints;
            base.ManaPoints = Consts.Mage.MANA_POINTS;
            base.Armor = DEFAULT_BODY_ARMOR;
            base.Weapon = DEFAULT_WEAPON;
            base.CharType = CharType.Arcane;
            base.IsAlive = true;
            base.Score = 0;

            counter++;
            CreationCofirmation();
        }


        // abstract method implementations
        public override int Attack()
        {
            return this.FireBall();
        }

        public override int SpecialAttack()
        {
            return this.FireWall();
        }

        public override int Defend()
        {
            return this.Meditation();
        }


        // class abilities
        public int FireBall()
        {
            return base.Weapon.Damage + 10;
        }

        public int FireWall()
        {
            throw new NotImplementedException();
        }

        public int Meditation()
        {
            return base.Armor.ArmorPoints + 5;
        }
    }
}