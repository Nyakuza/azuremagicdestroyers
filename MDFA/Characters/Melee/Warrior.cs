using Equipment;
using System;

namespace MDFA.Characters.Melee
{
    public class Warrior : Melee
    {

        private readonly Armor DEFAULT_BODY_ARMOR = new ChainLink();
        private readonly Weapon DEFAULT_WEAPON = new Sword();

 public Warrior()
            : this(Consts.Warrior.NAME, 1)
        {
        }

        public Warrior(string name, int level)
            : this(name, level, Consts.Warrior.HEALTH_POINTS)
        {
        }

        public Warrior(string name, int level, int healthPoints)
        {
            base.Name = name;
            base.Level = level;
            base.HealthPoints = healthPoints;
            base.AbilityPoints = Consts.Warrior.ABILITY_POINTS;
            base.Armor = DEFAULT_BODY_ARMOR;
            base.Weapon = DEFAULT_WEAPON;
            base.CharType = CharType.Melee;
            base.IsAlive = true;
            base.Score = 0;

            counter++;
            CreationCofirmation();
        }

        // abstract method implementations
        public override int Attack()
        {
            return this.Strike();
        }
        public override int SpecialAttack()
        {
            return this.Execute();
        }
        public override int Defend()
        {
            return this.SkinHarden();
        }


        // class abilities
        public int Strike()
        {
            return base.Weapon.Damage + 10;
        }
        public int Execute()
        {
            throw new NotImplementedException();
        }
        public int SkinHarden()
        {
            return base.Armor.ArmorPoints + 5;
        }
        public override void Move()
        {
            base.Move();
        }

    }
}