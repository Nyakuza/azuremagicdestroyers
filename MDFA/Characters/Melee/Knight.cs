using Equipment;
using System;

namespace MDFA.Characters.Melee
{
    public class Knight : Melee
    {

        private readonly Armor DEFAULT_BODY_ARMOR = new ChainLink();
        private readonly Hammer DEFAULT_WEAPON = new Hammer();
        
         public Knight()
            : this(Consts.Knight.NAME, 1)
        {
        }

        public Knight(string name, int level)
            : this(name, level, Consts.Knight.HEALTH_POINTS)
        {
        }

         public Knight(String name, int level, int healthPoints){
            base.Weapon = DEFAULT_WEAPON;
            base.Armor = DEFAULT_BODY_ARMOR;
            base.Name = name;
            base.Level = level;
            base.HealthPoints = healthPoints;
            base.AbilityPoints = Consts.Knight.ABILITY_POINTS;
            base.CharType = CharType.Melee;
            base.IsAlive = true;
            base.Score = 0;

            counter++;
            CreationCofirmation();
        }

        // abstract method implementations
        public override int Attack()
        {
            return this.HolyBlow();
        }

        public override int SpecialAttack()
        {
            return this.PurifySoul();
        }

        public override int Defend()
        {
            return this.RighteousWings();
        }


        // class abilities
        public int HolyBlow()
        {
            return base.Weapon.Damage + 10;
        }
        public int PurifySoul()
        {
            throw new NotImplementedException();
        }
        public int RighteousWings()
        {
            return base.Armor.ArmorPoints + 5;
        }
    }
}