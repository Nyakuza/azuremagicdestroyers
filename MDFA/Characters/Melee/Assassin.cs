using Equipment;
using System;

namespace MDFA.Characters.Melee
{
    public class Assassin : Melee
    {

        private readonly Armor DEFAULT_BODY_ARMOR = new LeatherVest();
        private readonly Sword DEFAULT_WEAPON = new Sword();

        public Assassin()
            : this(Consts.Assassin.NAME, 1)
        {
        }

        public Assassin(string name, int level)
            : this(name, level, Consts.Assassin.HEALTH_POINTS)
        {
        }

         public Assassin(String name, int level, int healthPoints){
            base.Name = name;
            base.Level = level;
            base.HealthPoints = healthPoints;
            base.AbilityPoints = Consts.Assassin.ABILITY_POINTS;
            base.Armor = DEFAULT_BODY_ARMOR;
            base.Weapon = DEFAULT_WEAPON;
            base.CharType = CharType.Melee;
            base.IsAlive = true;
            base.Score = 0;

            counter++;
            CreationCofirmation();
        }

        // abstract method implementations
        public override int Attack()
        {
            return this.Raze();
        }

        public override int SpecialAttack()
        {
            return this.BleedToDeath();
        }

        public override int Defend()
        {
            return this.Survival();
        }

        // class abilities
        public int Raze()
        {
            return base.Weapon.Damage + 10;
        }

        public int BleedToDeath()
        {
            throw new NotImplementedException();
        }

        public int Survival()
        {
            return base.Armor.ArmorPoints + 5;
        }
    }
}