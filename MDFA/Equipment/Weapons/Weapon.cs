using System;

namespace Equipment
{
    public class Weapon 
    {
        int damage;
        private static Random rnd = new Random();
        public int Damage { 
            get{ return damage; }
            set{ damage = value; }

        }

        public Random Rnd { 
            get{ return rnd; }

        }
    }
}