﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;


namespace MDFA
{

    public partial class Wrapper
    {
        [JsonProperty("JSONName")]
        public string Name { get; set; }

        [JsonProperty("JSONLevel")]
        public int Level { get; set; }

        [JsonProperty("JSONC_Class")]
        public string CClass { get; set; }

        [JsonProperty("JSONHealthPoints")]
        public int HealthPoints { get; set; }
    }
}
