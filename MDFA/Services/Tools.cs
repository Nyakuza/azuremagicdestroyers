﻿using System;

namespace MDFA
{
    public static class Tools
    {

        public static void ColorfulWriteLine(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ResetColor();
        }

        public static void TypeSpecificColorfulCW(string message, string type)
        {
            ConsoleColor color = ConsoleColor.White;

            switch (type)
            {
                case "MagicDestroyers.Characters.Melee.Warrior":
                    color = ConsoleColor.DarkBlue;
                    break;
                case "MagicDestroyers.Characters.Melee.Knight":
                    color = ConsoleColor.Blue;
                    break;
                case "MagicDestroyers.Characters.Melee.Assassin":
                    color = ConsoleColor.Cyan;
                    break;
                case "MagicDestroyers.Characters.Arcane.Mage":
                    color = ConsoleColor.Red;
                    break;
                case "MagicDestroyers.Characters.Arcane.Necromancer":
                    color = ConsoleColor.DarkRed;
                    break;
                case "MagicDestroyers.Characters.Arcane.Druid":
                    color = ConsoleColor.Red;
                    break;
                default:
                    color = ConsoleColor.White;
                    break;
            }

            Console.ForegroundColor = color;
            Console.WriteLine(message);
            Console.ResetColor();

        }
    }
}
