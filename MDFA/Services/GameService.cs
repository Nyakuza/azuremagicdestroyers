﻿using MDFA.Characters;
using MDFA.Characters.Arcane;
using MDFA.Characters.Melee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MDFA
{
    public static class GameService
    {

        static bool gameOver = false;
        static Random rng = new Random();
        //static Melee currentMelee;
        //static Arcane currentSpellcaster;

        static CharacterBase current_t1;
        static CharacterBase current_t2;

        public static string Play(List<CharacterBase> spellTeam, List<CharacterBase> meleeTeam)
        {
            while (!gameOver)
            {
                current_t1 = meleeTeam[rng.Next(0, meleeTeam.Count)];
                current_t2 = spellTeam[rng.Next(0, spellTeam.Count)];

                current_t2.TakeDamage(current_t1.Attack(), current_t1.Name, current_t1.GetType().ToString());

                if (!current_t2.IsAlive)
                {
                    current_t1.WonBattle();
                    spellTeam.Remove(current_t2);

                    if (spellTeam.Count == 0)
                    {
                        Tools.ColorfulWriteLine("\nMelee team wins!", ConsoleColor.Red);
                        //log.LogInformation("TEAM 1 WINS!");
                        return "Team 1 Wins!";
                        break;
                    }
                    else
                    {
                        current_t2 = spellTeam[rng.Next(0, spellTeam.Count)];
                    }
                }

                current_t1.TakeDamage(current_t2.Attack(), current_t2.Name, current_t2.GetType().ToString());

                if (!current_t1.IsAlive)
                {
                    current_t2.WonBattle();
                    meleeTeam.Remove(current_t1);

                    if (meleeTeam.Count == 0)
                    {

                        Tools.ColorfulWriteLine("\nSpell team wins!", ConsoleColor.Red);
                        //log.LogInformation("TEAM 2 WINS!");
                        return "Team 2 Wins";
                        break;
                    }
                    else
                    {
                        current_t1 = meleeTeam[rng.Next(0, meleeTeam.Count)];
                    }
                }
            }
            return "Error has occurred";
        }

        public static Tuple<List<CharacterBase>, List<CharacterBase>> FillTeams(List<Wrapper> charListWrapper)
        {
            List<CharacterBase> team_1 = new List<CharacterBase>();
            List<CharacterBase> team_2 = new List<CharacterBase>();
            int i = 0;

            foreach (var c in charListWrapper)
            {

                switch (c.CClass)
                {
                    case "Warrior":
                        {
                            if (i <= 2)
                            {
                                team_1.Add(new Warrior(c.Name, c.Level));
                            }
                            else
                            {
                                team_2.Add(new Warrior(c.Name, c.Level));
                            }
                            break;
                        }
                    case "Knight":
                        {
                            if (i <= 2)
                            {
                                team_1.Add(new Knight(c.Name, c.Level));
                            }
                            else
                            {
                                team_2.Add(new Knight(c.Name, c.Level));
                            }
                            break;
                        }
                    case "Assassin":
                        {
                            if (i <= 2)
                            {
                                team_1.Add(new Assassin(c.Name, c.Level));
                            }
                            else
                            {
                                team_2.Add(new Assassin(c.Name, c.Level));
                            }
                            break;
                        }
                    case "Mage":
                        {
                            if (i <= 2)
                            {
                                team_1.Add(new Mage(c.Name, c.Level));
                            }
                            else
                            {
                                team_2.Add(new Mage(c.Name, c.Level));
                            }
                            break;
                        }
                    case "Necromancer":
                        {
                            if (i <= 2)
                            {
                                team_1.Add(new Necromancer(c.Name, c.Level));
                            }
                            else
                            {
                                team_2.Add(new Necromancer(c.Name, c.Level));
                            }
                            break;
                        }
                    case "Druid":
                        {
                            if (i <= 2)
                            {
                                team_1.Add(new Druid(c.Name, c.Level));
                            }
                            else
                            {
                                team_2.Add(new Druid(c.Name, c.Level));
                            }
                            break;
                        }
                    default:
                        {
                            if (i <= 2)
                            {
                                team_1.Add(new Warrior(c.Name, c.Level));
                            }
                            else
                            {
                                team_2.Add(new Warrior(c.Name, c.Level));
                            }
                            break;
                        }
                }
                i++;
            }

            return Tuple.Create(team_1, team_2);
            

        }

    }
}