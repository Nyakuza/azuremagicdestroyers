﻿using System;
using System.Data.SqlClient;
using System.Text;

namespace MDFA
{
    public static class SQLService
    {
        public static void DBCheck()
        {
            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                builder.DataSource = "throwaydbserver.database.windows.net";
                builder.UserID = "nyakuza";
                builder.Password = "PASSword02";
                builder.InitialCatalog = "throwawaydbtest";

                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    Console.WriteLine("\nQuery data example:");
                    Console.WriteLine("=========================================\n");

                    connection.Open();

                    String sql = "INSERT INTO outcomes (Result) VALUES ('FATEST');";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine("{0} {1}", reader.GetString(0), reader.GetString(1));
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.WriteLine("\nDone. Press enter.");
            Console.ReadLine();
        }
    }
}